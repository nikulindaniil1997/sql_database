Use Task1
create table Employees(
	 ID int IDENTITY(1,1) PRIMARY KEY,
	 FirstName varchar(255),
	 LastName varchar(255) NOT NULL,
)

create table Project( 
	ID int IDENTITY(1,1) PRIMARY KEY,
	ProjectName varchar(255) NOT NULL,
	createData date,
	closedData date,
	closed bit
)

create table Employees_positions(
	ID int IDENTITY(1,1) PRIMARY KEY,
	positionsname varchar(255)
)
create table Task_status(
	ID int IDENTITY(1,1) PRIMARY KEY,
	st varchar(255) 
)

create table Task(
	ID int IDENTITY(1,1) PRIMARY KEY,
	employees_id int FOREIGN KEY REFERENCES Employees(ID),
	project_id int FOREIGN KEY REFERENCES Project(ID),
	status_id int FOREIGN KEY REFERENCES Task_status(ID),
	startdate date,
	deadline date
)

create table  Employees_In_Project_position(
	employees_id int FOREIGN KEY REFERENCES Employees(ID),
	project_id int FOREIGN KEY REFERENCES Project(ID),
	employeesposition_id int FOREIGN KEY REFERENCES Employees_positions(ID)
)
