use Task1
	--1
	Select Employees_positions.positionsname AS Positions,Count(employeesposition_id) as Number_Of_Employees_On_Position 
	From Employees_In_Project_position 
	Left join Employees_positions On Employees_In_Project_position.employeesposition_id =Employees_positions.ID
	Group by positionsname 
	--2
	Select  Employees_positions.positionsname AS Positions,Count(employeesposition_id) as Number_Of_Employees_On_Position 
	From Employees_In_Project_position 
	Right join Employees_positions On Employees_In_Project_position.employeesposition_id =Employees_positions.ID
	Group by positionsname
	Having  Count(employeesposition_id) = 0
	--3

	Select 
	ProjectName as Names,
	Count(employeesposition_id) as Number_Of_Employees_On_Position,
	positionsname As Positions
	From Project p
	join Employees_In_Project_position epp ON epp.project_id = p.ID
	join Employees_positions ep On epp.employeesposition_id =ep.ID
	Group by positionsname,ProjectName
	--4

	Select 
	p.ID,
	p.ProjectName,
	(cast(count (Distinct t.ID)as decimal(18,2))/cast(count (Distinct epp.employees_id)as decimal(3,1)))as TPP
	From Project p
	join Employees_In_Project_position epp ON epp.project_id = p.ID
	join Task t ON t.employees_id = epp.employees_id
	Group By p.ID,p.ProjectName

	--5
	
	Select ProjectName, DATEDIFF(day,createData,closedData)AS minutes_for_project From Project
	 
	--6
		Select 
		e.FirstName,
		e.LastName,
		Count (e.ID) as l
		From Employees e 
		left join Task t ON t.employees_id = e.ID
		Where t.status_id !=2
		Group by e.FirstName,e.LastName
		Having Count (e.ID)  =  (Select top 1 Count (e.ID) as l From Employees e left join Task t ON t.employees_id = e.ID Where t.status_id !=2 Group by e.ID order by l)
	--7
		Select 
		e.ID,
		e.FirstName,
		e.LastName,
		Count (e.ID) as l
		From Employees e 
		left join Task t ON t.employees_id = e.ID
		Where t.status_id !=2 AND t.deadline < GETDATE()
		Group by e.ID,e.FirstName,e.LastName
		Having Count (e.ID)  =  (Select top 1 Count (e.ID) as l From Employees e left join Task t ON t.employees_id = e.ID Where t.status_id !=2 AND t.deadline < GETDATE() Group by e.ID order by l desc)

	--8
	Update Task
	Set deadline= DATEADD(day, 5 ,deadline)
	Where status_id != 2;
	--9

	Select 
		p.ProjectName,
		COUNT(t.ID)
	From Task t
	Join Project p ON t.project_id = p.ID
	Where t.status_id = 1
	Group by ProjectName
--10
	go
	CREATE TRIGGER closedTrigger  
	ON Task
	AFTER UPDATE   
	AS
		IF ( UPDATE (status_id) )
		BEGIN
			DECLARE @status_id INT	
			SELECT
			  @status_id = inserted.status_id
			FROM
			  inserted
			  IF(@status_id = 2)
				BEGIN
					UPdate Project 
					set closed = 1, createData = GETDATE(),closedData = GETDATE()
					where ID IN(
					select ID from Project 
					where ID not in (select p.ID from Project p join Task t on p.ID = t.project_id where t.status_id != 2))
				END;
		END;
	GO  
	--11
		select distinct FirstName,LastName,e.ID from Project p 
		join Task t on p.ID = t.project_id 
		join Employees e on t.employees_id=e.ID
		where t.status_id = 2
	