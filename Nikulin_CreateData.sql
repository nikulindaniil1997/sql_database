Use Task1
Insert into Employees values('FirstName','LastName'),('Boris','Sokolovsky'),('Ivan','Ivanov'),('Petr','Petrov'),('Nikita','Nikitin'),('Ivan','Pupkin');
Insert into Project values ('Borsh','17/12/2020','17/12/2021','false'),
							('Salo','11/11/2019','12/12/2022','false'),
							('Beer','9/11/2001','1/12/2020','true');
Insert into Task_status values ('Open'),('Closed'),('Taken'),('Revision');
Insert into Employees_positions values ('Team Ledaer'),('Developer'),('Tester'),('HR'),('Cleaning manager');
Insert into Task values (2,1,3,'17/12/2020','30/6/2021'),
						(3,2,4,'1/1/2020','30/12/2020'),
						(4,3,2,'20/10/2019','30/1/2020')
						,(2,1,1,'20/10/2019','30/1/2020')
						,(1,1,1,'11/11/2013','30/1/2023')
						,(5,1,2,'11/11/2013','30/1/2023')
Insert into Employees_In_Project_position values (1,1,4),(2,1,2),(2,2,1),(3,3,3),(4,3,1),(5,2,4),(6,3,2)
